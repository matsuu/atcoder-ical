# atcoder-ical

Generate ical file for [AtCoder](https://atcoder.jp/contests/).

## Usage

```
go get gitlab.com/matsuu/atcoder-ical
atcoder-ical > atcoder.ics
```

## Ref

* https://gitlab.com/matsuu/atcoder-ical
* https://matsuu.gitlab.io/atcoder-ical/atcoder.ics
