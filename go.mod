module gitlab.com/matsuu/atcoder-ical

go 1.12

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/lestrrat-go/bufferpool v0.0.0-20180220091733-e7784e1b3e37 // indirect
	github.com/lestrrat-go/ical v0.0.0-20190317233631-91af071bafbc
	github.com/pkg/errors v0.8.1 // indirect
)
