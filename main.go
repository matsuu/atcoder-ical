package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/lestrrat-go/ical"
)

const (
	baseURL           = "https://atcoder.jp"
	calTitle          = "AtCoder コンテスト"
	timeParseFormat   = "2006-01-02 15:04:05-0700"
	timeIcalFormat    = "20060102T150405"
	timeIcalUTCFormat = "20060102T150405Z"
	timeZone          = "Asia/Tokyo"
)

func getDocument(url string) (*goquery.Document, error) {
	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("status code error: %d %s", res.StatusCode, res.Status)
	}

	return goquery.NewDocumentFromReader(res.Body)
}

func outputIcal(w io.Writer) {
	var err error
	var doc *goquery.Document

	url := baseURL + "/contests/?lang=ja"
	for i := 1; i <= 3; i++ {
		doc, err = getDocument(url)
		if err != nil {
			log.Print(err)
			time.Sleep(time.Duration(i * 5) * time.Second)
			continue
		}
		break
	}
	if err != nil {
		log.Fatal(err)
	}

	c := ical.New()
	c.AddProperty("X-WR-CALNAME", calTitle)
	c.AddProperty("X-WR-TIMEZONE", timeZone)
	c.AddProperty("METHOD", "PUBLISH")

	tz := ical.NewTimezone()
	tz.AddProperty("TZID", timeZone)

	std := ical.NewStandard()
	std.AddProperty("dtstart", "19700101T000000")
	std.AddProperty("tzoffsetfrom", "+0900")
	std.AddProperty("tzoffsetto", "+0900")

	tz.AddEntry(std)
	c.AddEntry(tz)

	tzid := ical.WithParameters(ical.Parameters{"tzid": []string{timeZone}})

	doc.Find("h3:not(:contains('常設中')) + div tr:has(td)").Each(func(i int, s *goquery.Selection) {
		start, err := time.Parse(timeParseFormat, s.Find("td:nth-child(1)").Text())
		if err != nil {
			log.Println(err)
			return
		}
		title := s.Find("td:nth-child(2) a").Text()
		path, ok := s.Find("td:nth-child(2) a").Attr("href")
		if !ok {
			log.Println("href not found")
			return
		}
		// hrefはルートパスになっているのでURLに変換
		url := fmt.Sprintf("%s%s", baseURL, path)

		duration, err := time.ParseDuration(strings.Replace(s.Find("td:nth-child(3)").Text(), ":", "h", -1) + "m")
		if err != nil {
			log.Println(err)
			return
		}

		rating := s.Find("td:nth-child(4)").Text()

		e := ical.NewEvent()
		e.AddProperty("dtstart", start.Format(timeIcalFormat), tzid)
		e.AddProperty("dtend", start.Add(duration).Format(timeIcalFormat), tzid)
		e.AddProperty("dtstamp", time.Now().UTC().Format(timeIcalUTCFormat))
		e.AddProperty("uid", url)
		e.AddProperty("summary", title)
		e.AddProperty("url", url)
		e.AddProperty("description", fmt.Sprintf("Rated対象: %s\nURL: %s", rating, url))

		// color
		color, ok := s.Find("td:nth-child(2) span").Attr("class")
		if ok && strings.HasPrefix(color, "user-") {
			color = strings.TrimPrefix(color, "user-")
			e.AddProperty("COLOR", color, ical.WithForce(true))
		}

		c.AddEntry(e)
	})
	ical.NewEncoder(w).Encode(c)
}

func main() {
	outputIcal(os.Stdout)
}
