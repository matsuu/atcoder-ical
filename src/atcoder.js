import { Calendar } from '@fullcalendar/core';
import jaLocale from '@fullcalendar/core/locales/ja';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';

import ICAL from 'ical.js';

import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';
import '@fullcalendar/timegrid/main.css';
import '@fullcalendar/list/main.css';

import './atcoder.css';

function component() {
  var element = document.createElement('div');

  var calendar = new Calendar(element, {
    locale: jaLocale,
    plugins: [ interactionPlugin, dayGridPlugin, timeGridPlugin, listPlugin ],
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
    },
    navLinks: true,
  });

  fetch('atcoder.ics').then(response => {
    return response.text();
  }).then(text => {
    var events = [];
    var jcal = ICAL.parse(text);
    for (event of new ICAL.Component(jcal).getAllSubcomponents('vevent')) {
      var ev = {
        id: event.getFirstPropertyValue('uid'),
        title: event.getFirstPropertyValue('summary'),
        url: event.getFirstPropertyValue('url'),
        start: event.getFirstPropertyValue('dtstart').toJSDate(),
        end: event.getFirstPropertyValue('dtend').toJSDate(),
        color: event.getFirstPropertyValue('color') || 'black',
      };
      events.push(ev);
    }
    calendar.addEventSource(events);
    calendar.render();
  });

  element.classList.add('calendar');

  return element;
}

document.body.appendChild(component());

