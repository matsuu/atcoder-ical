const path = require('path')

module.exports = {
  mode: 'production',
  entry: './src/atcoder.js',
  output: {
    filename: 'atcoder.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      }
    ]
  }
}
